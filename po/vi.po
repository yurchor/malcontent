# Vietnamese translations for malcontent package.
# Copyright (C) 2020 THE malcontent'S COPYRIGHT HOLDER
# This file is distributed under the same license as the malcontent package.
# Automatically generated, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: malcontent\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-06-10 12:04+0100\n"
"PO-Revision-Date: 2020-04-15 13:42+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: accounts-service/com.endlessm.ParentalControls.policy.in:4
msgid "Change your own app filter"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:5
msgid "Authentication is required to change your app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:14
msgid "Read your own app filter"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:15
msgid "Authentication is required to read your app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:24
msgid "Change another user’s app filter"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:25
msgid "Authentication is required to change another user’s app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:34
msgid "Read another user’s app filter"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:35
msgid "Authentication is required to read another user’s app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:44
msgid "Change your own session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:45
msgid "Authentication is required to change your session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:54
msgid "Read your own session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:55
msgid "Authentication is required to read your session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:64
msgid "Change another user’s session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:65
msgid "Authentication is required to change another user’s session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:74
msgid "Read another user’s session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:75
msgid "Authentication is required to read another user’s session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:84
msgid "Change your own account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:85
msgid "Authentication is required to change your account info."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:94
msgid "Read your own account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:95
msgid "Authentication is required to read your account info."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:104
msgid "Change another user’s account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:105
msgid "Authentication is required to change another user’s account info."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:114
msgid "Read another user’s account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:115
msgid "Authentication is required to read another user’s account info."
msgstr ""

#: libmalcontent/app-filter.c:694
#, c-format
msgid "App filter for user %u was in an unrecognized format"
msgstr ""

#: libmalcontent/app-filter.c:725
#, c-format
msgid "OARS filter for user %u has an unrecognized kind ‘%s’"
msgstr ""

#: libmalcontent/manager.c:283 libmalcontent/manager.c:412
#, c-format
msgid "Not allowed to query app filter data for user %u"
msgstr ""

#: libmalcontent/manager.c:288
#, c-format
msgid "User %u does not exist"
msgstr ""

#: libmalcontent/manager.c:394
msgid "App filtering is globally disabled"
msgstr ""

#: libmalcontent/manager.c:777
msgid "Session limits are globally disabled"
msgstr ""

#: libmalcontent/manager.c:795
#, c-format
msgid "Not allowed to query session limits data for user %u"
msgstr ""

#: libmalcontent/session-limits.c:306
#, c-format
msgid "Session limit for user %u was in an unrecognized format"
msgstr ""

#: libmalcontent/session-limits.c:328
#, c-format
msgid "Session limit for user %u has an unrecognized type ‘%u’"
msgstr ""

#: libmalcontent/session-limits.c:346
#, c-format
msgid "Session limit for user %u has invalid daily schedule %u–%u"
msgstr ""

#. TRANSLATORS: This is the formatting of English and localized name
#. of the rating e.g. "Adults Only (solo adultos)"
#: libmalcontent-ui/gs-content-rating.c:75
#, c-format
msgid "%s (%s)"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:209
msgid "General"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:218
msgid "ALL"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:222
#: libmalcontent-ui/gs-content-rating.c:485
msgid "Adults Only"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:224
#: libmalcontent-ui/gs-content-rating.c:484
msgid "Mature"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:226
#: libmalcontent-ui/gs-content-rating.c:483
msgid "Teen"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:228
#: libmalcontent-ui/gs-content-rating.c:482
msgid "Everyone 10+"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:230
#: libmalcontent-ui/gs-content-rating.c:481
msgid "Everyone"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:232
#: libmalcontent-ui/gs-content-rating.c:480
msgid "Early Childhood"
msgstr ""

#. Translators: the placeholder is a user’s full name
#: libmalcontent-ui/restrict-applications-dialog.c:222
#, c-format
msgid "Restrict %s from using the following installed applications."
msgstr ""

#: libmalcontent-ui/restrict-applications-dialog.ui:6
#: libmalcontent-ui/restrict-applications-dialog.ui:12
msgid "Restrict Applications"
msgstr ""

#: libmalcontent-ui/restrict-applications-selector.ui:24
msgid "No applications found to restrict."
msgstr ""

#. Translators: this is the full name for an unknown user account.
#: libmalcontent-ui/user-controls.c:242 libmalcontent-ui/user-controls.c:253
msgid "unknown"
msgstr ""

#: libmalcontent-ui/user-controls.c:338 libmalcontent-ui/user-controls.c:425
#: libmalcontent-ui/user-controls.c:711
msgid "All Ages"
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:514
#, c-format
msgid ""
"Prevents %s from running web browsers. Limited web content may still be "
"available in other applications."
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:519
#, c-format
msgid "Prevents specified applications from being used by %s."
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:524
#, c-format
msgid "Prevents %s from installing applications."
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:529
#, c-format
msgid "Applications installed by %s will not appear for other users."
msgstr ""

#: libmalcontent-ui/user-controls.ui:17
msgid "Application Usage Restrictions"
msgstr ""

#: libmalcontent-ui/user-controls.ui:68
msgid "Restrict _Web Browsers"
msgstr ""

#: libmalcontent-ui/user-controls.ui:152
msgid "_Restrict Applications"
msgstr ""

#: libmalcontent-ui/user-controls.ui:231
msgid "Software Installation Restrictions"
msgstr ""

#: libmalcontent-ui/user-controls.ui:281
msgid "Restrict Application _Installation"
msgstr ""

#: libmalcontent-ui/user-controls.ui:366
msgid "Restrict Application Installation for _Others"
msgstr ""

#: libmalcontent-ui/user-controls.ui:451
msgid "Application _Suitability"
msgstr ""

#: libmalcontent-ui/user-controls.ui:473
msgid ""
"Restricts browsing or installation of applications to applications suitable "
"for certain ages or above."
msgstr ""

#. Translators: This is the title of the main window
#. Translators: the name of the application as it appears in a software center
#: malcontent-control/application.c:105 malcontent-control/main.ui:12
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:9
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:3
msgid "Parental Controls"
msgstr ""

#: malcontent-control/application.c:270
msgid "Copyright © 2019, 2020 Endless Mobile, Inc."
msgstr ""

#. Translators: this should be "translated" to the
#. names of people who have translated Malcontent into
#. this language, one per line.
#: malcontent-control/application.c:275
msgid "translator-credits"
msgstr ""

#. Translators: "Malcontent" is the brand name of this
#. project, so should not be translated.
#: malcontent-control/application.c:281
msgid "Malcontent Website"
msgstr ""

#: malcontent-control/application.c:299
msgid "The help contents could not be displayed"
msgstr ""

#: malcontent-control/application.c:336
msgid "Failed to load user data from the system"
msgstr ""

#: malcontent-control/application.c:338
msgid "Please make sure that the AccountsService is installed and enabled."
msgstr ""

#: malcontent-control/carousel.ui:48
msgid "Previous Page"
msgstr ""

#: malcontent-control/carousel.ui:74
msgid "Next Page"
msgstr ""

#: malcontent-control/main.ui:93
msgid "Permission Required"
msgstr ""

#: malcontent-control/main.ui:107
msgid ""
"Permission is required to view and change user parental controls settings."
msgstr ""

#: malcontent-control/main.ui:148
msgid "No Child Users Configured"
msgstr ""

#: malcontent-control/main.ui:162
msgid ""
"No child users are currently set up on the system. Create one before setting "
"up their parental controls."
msgstr ""

#: malcontent-control/main.ui:174
msgid "Create _Child User"
msgstr ""

#: malcontent-control/main.ui:202
msgid "Loading…"
msgstr ""

#: malcontent-control/main.ui:265
msgid "_Help"
msgstr "Trợ _giúp"

#: malcontent-control/main.ui:269
msgid "_About Parental Controls"
msgstr ""

#. Translators: the brief summary of the application as it appears in a software center.
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:12
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:4
msgid "Set parental controls and monitor usage by users"
msgstr ""

#. Translators: These are the application description paragraphs in the AppData file.
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:16
msgid ""
"Manage users’ parental controls restrictions, controlling how long they can "
"use the computer for, what software they can install, and what installed "
"software they can run."
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:39
msgid "The GNOME Project"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:50
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:59
msgid "Minor improvements to parental controls application UI"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:51
msgid "Add a user manual"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:52
msgid "Translation updates"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:60
msgid "Translations to Ukrainian and Polish"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:67
msgid "Improve parental controls application UI and add icon"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:68
msgid "Support for indicating which accounts are parent accounts"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:75
msgid "Initial release of basic parental controls application"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:76
msgid "Support for setting app installation and run restrictions on users"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:83
msgid "Maintenance release of underlying parental controls library"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:7
msgid "org.freedesktop.MalcontentControl"
msgstr ""

#. Translators: Search terms to find this application. Do NOT translate or localise the semicolons! The list MUST also end with a semicolon!
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:13
msgid ""
"parental controls;screen time;app restrictions;web browser restrictions;oars;"
"usage;usage limit;kid;child;"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.policy.in:9
msgid "Manage parental controls"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.policy.in:10
msgid "Authentication is required to read and change user parental controls"
msgstr ""

#: malcontent-control/user-selector.c:426
msgid "Your account"
msgstr "Tài khoản của bạn"

#. Always allow root, to avoid a situation where this PAM module prevents
#. * all users logging in with no way of recovery.
#: pam/pam_malcontent.c:142 pam/pam_malcontent.c:188
#, c-format
msgid "User ‘%s’ has no time limits enabled"
msgstr ""

#: pam/pam_malcontent.c:151 pam/pam_malcontent.c:172
#, c-format
msgid "Error getting session limits for user ‘%s’: %s"
msgstr ""

#: pam/pam_malcontent.c:182
#, c-format
msgid "User ‘%s’ has no time remaining"
msgstr ""

#: pam/pam_malcontent.c:200
#, c-format
msgid "Error setting time limit on login session: %s"
msgstr ""

#~ msgid "No cartoon violence"
#~ msgstr "Không có nội dung bạo lực hoạt họa"

#~ msgid "Cartoon characters in unsafe situations"
#~ msgstr "Các nhân vật hoạt hình trong các tình huống nguy hiểm"

#~ msgid "Cartoon characters in aggressive conflict"
#~ msgstr "Các nhân vật hoạt hình với mâu thuẫn dữ dội"

#~ msgid "Graphic violence involving cartoon characters"
#~ msgstr "Đồ họa cảnh bạo lực với các nhân vật hoạt hình"

#~ msgid "No fantasy violence"
#~ msgstr "Không có nội dung bạo lực giả tưởng"

#~ msgid "Characters in unsafe situations easily distinguishable from reality"
#~ msgstr ""
#~ "Các nhân vật trong các tình huống không an toàn dễ dàng xa rời thực tế"

#~ msgid ""
#~ "Characters in aggressive conflict easily distinguishable from reality"
#~ msgstr ""
#~ "Các nhân vật có mâu thuẫn dữ dội có thể dễ dàng phân biệt với thực tế"

#~ msgid "Graphic violence easily distinguishable from reality"
#~ msgstr "Bạo lực đồ họa dễ dàng phân biệt với thực tế"

#~ msgid "No realistic violence"
#~ msgstr "Không có nội dung bạo lực thực tế"

#~ msgid "Mildly realistic characters in unsafe situations"
#~ msgstr "Các nhân vật thực tế ôn hòa trong các tình huống không an toàn"

#~ msgid "Depictions of realistic characters in aggressive conflict"
#~ msgstr "Miêu tả nhân vật thực tế trong trạng thái vô cùng mâu thuẫn"

#~ msgid "Graphic violence involving realistic characters"
#~ msgstr "Đồ họa cảnh bạo lực với các nhân vật thực tế"

#~ msgid "No bloodshed"
#~ msgstr "Không có nội dung chém giết"

#~ msgid "Unrealistic bloodshed"
#~ msgstr "Cảnh giết chóc phi thực tế"

#~ msgid "Realistic bloodshed"
#~ msgstr "Sự giết chóc thực tế"

#~ msgid "Depictions of bloodshed and the mutilation of body parts"
#~ msgstr "Diễn tả cảnh đổ máu và phanh thây"

#~ msgid "No sexual violence"
#~ msgstr "Không bạo lực tình dụng"

#~ msgid "Rape or other violent sexual behavior"
#~ msgstr "Cưỡng hiếp hoặc hành vi tình dục bạo lực khác"

#~ msgid "No references to alcohol"
#~ msgstr "Không nhắc đến rượu bia"

#~ msgid "References to alcoholic beverages"
#~ msgstr "Dẫn chiếu đến các thức uống có cồn"

#~ msgid "Use of alcoholic beverages"
#~ msgstr "Sử dụng các đồ uống có cồn"

#~ msgid "No references to illicit drugs"
#~ msgstr "Không nhắc đến thuốc cấm"

#~ msgid "References to illicit drugs"
#~ msgstr "Nhắc đến các loại thuốc cấm"

#~ msgid "Use of illicit drugs"
#~ msgstr "Sử dụng ma túy trái phép"

#~ msgid "References to tobacco products"
#~ msgstr "Dẫn chiếu đến các sản phẩm thuốc lá"

#~ msgid "Use of tobacco products"
#~ msgstr "Sử dụng các sản phẩm thuốc lá"

#~ msgid "No nudity of any sort"
#~ msgstr "Không có bất kỳ loại nội dung khỏa thân nào"

#~ msgid "Brief artistic nudity"
#~ msgstr "Ảnh khoả thân nghệ thuật ngắn"

#~ msgid "Prolonged nudity"
#~ msgstr "Ảnh khoả thân kéo dài"

#~ msgid "No references or depictions of sexual nature"
#~ msgstr "Không dẫn chiếu hoặc mô tả bản năng tình dục"

#~ msgid "Provocative references or depictions"
#~ msgstr "Các dẫn chiếu hoặc mô tả mang tính khiêu khích"

#~ msgid "Sexual references or depictions"
#~ msgstr "Diễn tả hoặc nhắc đến nội dung tình dục"

#~ msgid "Graphic sexual behavior"
#~ msgstr "Đồ họa hành vi giới tính"

#~ msgid "No profanity of any kind"
#~ msgstr "Không có loại nội dung báng bổ nào"

#~ msgid "Mild or infrequent use of profanity"
#~ msgstr "Sử dụng lời lẽ tục tĩu ở mức nhẹ hoặc không thường xuyên"

#~ msgid "Moderate use of profanity"
#~ msgstr "Sử dụng từ ngữ thô tục ở mức trung bình"

#~ msgid "Strong or frequent use of profanity"
#~ msgstr "Sử dụng từ ngữ thô tục ở mức mạnh hay thường xuyên"

#~ msgid "No inappropriate humor"
#~ msgstr "Không đùa cợt không thích hợp"

#~ msgid "Slapstick humor"
#~ msgstr "Hài hước vui nhộn"

#~ msgid "Vulgar or bathroom humor"
#~ msgstr "Đùa cợt tục tĩu hoặc khiếm nhã"

#~ msgid "Mature or sexual humor"
#~ msgstr "Đùa cợt người lớn hoặc về giới tính"

#~ msgid "No discriminatory language of any kind"
#~ msgstr "Không có dạng ngôn ngữ mang tính kỳ thị nào"

#~ msgid "Negativity towards a specific group of people"
#~ msgstr "Tính chất cấm đoán đối với một nhóm người cụ thể"

#~ msgid "Discrimination designed to cause emotional harm"
#~ msgstr "Sự phân biệt đối xử được tạo ra để gây tổn thương tinh thần"

#~ msgid "Explicit discrimination based on gender, sexuality, race or religion"
#~ msgstr ""
#~ "Sự phân biệt đối xử rõ rệt về giới tính, thiên hướng tình dục, sắc tộc "
#~ "hoặc tôn giáo"

#~ msgid "No advertising of any kind"
#~ msgstr "Không quảng cáo dưới mọi hình thức"

#~ msgid "Product placement"
#~ msgstr "Quảng Cáo Sản Phẩm"

#~ msgid "Explicit references to specific brands or trademarked products"
#~ msgstr ""
#~ "Dẫn chiếu rõ rệt đến các thương hiệu hoặc sản phẩm có nhãn hiệu cụ thể"

#~ msgid "No gambling of any kind"
#~ msgstr "Không có dạng nội dung cờ bạc nào"

#~ msgid "Gambling on random events using tokens or credits"
#~ msgstr "Cờ bạc về các sự kiện ngẫu nhiên bằng hiện vật hoặc thẻ tín dụng"

#~ msgid "Gambling using “play” money"
#~ msgstr "Chơi cờ bạc bằng tiền \"ảo\""

#~ msgid "Gambling using real money"
#~ msgstr "Đánh bạc bằng tiền thật"

#~ msgid "No ability to spend money"
#~ msgstr "Không có khả năng tiêu tiền"

#~ msgid "Ability to spend real money in-game"
#~ msgstr "Khả năng tiêu tiền thật trong trò chơi"

#~ msgid "No sharing of social network usernames or email addresses"
#~ msgstr ""
#~ "Không chia sẻ tên người dùng hay địa chỉ email dùng trên mạng xã hội"

#~ msgid "Sharing social network usernames or email addresses"
#~ msgstr "Chia sẻ tên người dùng mạng xã hội hoặc địa chỉ email"

#~ msgid "No sharing of user information with 3rd parties"
#~ msgstr "Không chia sẻ thông tin người dùng với các bên thứ ba"

#~ msgid "No sharing of physical location to other users"
#~ msgstr "Không chia sẻ vị trí thực tế với người dùng khác"

#~ msgid "Sharing physical location to other users"
#~ msgstr "Chia sẻ địa điểm vật lý với người dùng khác"
